package westphilidalphia.botinator.emoji;

import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.event.domain.message.ReactionRemoveEvent;
import reactor.core.publisher.Mono;

public interface HookableEmoji {
    Mono<Void> triggerAdded(ReactionAddEvent event);

    Mono<Void> triggerRemoved(ReactionRemoveEvent event);
}
