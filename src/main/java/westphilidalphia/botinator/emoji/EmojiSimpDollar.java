package westphilidalphia.botinator.emoji;

import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.event.domain.message.ReactionRemoveEvent;
import discord4j.core.object.entity.User;
import reactor.core.publisher.Mono;
import westphilidalphia.botinator.Botinator;

public class EmojiSimpDollar implements HookableEmoji {
    @Override
    public Mono<Void> triggerAdded(ReactionAddEvent event) {
        return event.getMessage()
                .doOnNext(message -> message.getAuthor()
                        .ifPresent(discordUser -> transactionWrapper(discordUser, 1)))
                .then();
    }

    @Override
    public Mono<Void> triggerRemoved(ReactionRemoveEvent event) {
        return event.getMessage()
                .doOnNext(message -> message.getAuthor()
                        .ifPresent(discordUser -> transactionWrapper(discordUser, -1)))
                .then();
    }

    private void transactionWrapper(User discordUser, int amount) {
        Botinator.userService.simpDollarTransaction(discordUser.getId(), amount);
    }
}
