package westphilidalphia.botinator;

import com.merakianalytics.orianna.Orianna;
import com.merakianalytics.orianna.types.common.Region;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.event.domain.message.ReactionRemoveEvent;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import westphilidalphia.botinator.commands.*;
import westphilidalphia.botinator.emoji.EmojiSimpDollar;
import westphilidalphia.botinator.emoji.HookableEmoji;
import westphilidalphia.botinator.models.Keyword;
import westphilidalphia.botinator.models.User;
import westphilidalphia.botinator.services.KeywordService;
import westphilidalphia.botinator.services.UserService;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Botinator {
    public final static UserService userService = new UserService();
    public final static KeywordService keywordService = new KeywordService();
    public final static SessionFactory sessionFactory;
    private final static Map<String, RunnableCommand> commandMap = new HashMap<>();
    private final static Map<String, HookableEmoji> emojiHookMap = new HashMap<>();
    private final static String commandPrefix = "%";

    static {
        commandMap.put("ping", new CommandPing());
        commandMap.put("simpbank", new CommandSimpBank());
        commandMap.put("magicconch", new CommandMagicConch());
        //TODO: turn command below into a scheduled event
        commandMap.put("losingstreak", new CommandLosingStreak());
        commandMap.put("istheshipstillstuck", new CommandShipStatus());
        emojiHookMap.put("simpdollar", new EmojiSimpDollar());

        Orianna.setRiotAPIKey(System.getenv("RIOT_API_KEY"));
        Orianna.setDefaultRegion(Region.NORTH_AMERICA);

        Configuration hibernateConfig = new Configuration();

        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "org.postgresql.Driver");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");
        settings.put(Environment.URL, System.getenv("JDBC_DATABASE_URL"));

        hibernateConfig.setProperties(settings);

        StandardServiceRegistry standardServiceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(hibernateConfig.getProperties()).build();

        MetadataSources sources = new MetadataSources(standardServiceRegistry);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Keyword.class);
        Metadata metadata = sources.getMetadataBuilder().build();

        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    public static void main(String[] args) {
        new Botinator().start();
    }

    private void start() {
        DiscordClientBuilder.create(System.getenv("DISCORD_API_KEY"))
                .build()
                .withGateway(gateway -> {
                    Mono<Void> baddaBing = gateway.getEventDispatcher().on(MessageCreateEvent.class)
                            .flatMap(event -> Mono.justOrEmpty(event.getMessage().getContent())
                                    .filter(s -> s.equalsIgnoreCase("badda bing"))
                                    .flatMap(s -> event.getMessage().getAuthorAsMember()
                                            .flatMap(member -> event.getMessage().getChannel()
                                                    .flatMap(messageChannel -> messageChannel
                                                            .createMessage("<@" + member.getId().asString() + "> badda boom")))))
                            .then();

                    Mono<Void> commandListener = gateway.getEventDispatcher().on(MessageCreateEvent.class)
                            .flatMap(event -> Mono.justOrEmpty(event.getMessage().getContent())
                                    .flatMap(content -> Flux.fromIterable(commandMap.entrySet())
                                            .filter(entry -> content.startsWith(commandPrefix + entry.getKey()))
                                            .flatMap(entry -> entry.getValue().run(event))
                                            .next()))
                            .then();

                    Mono<Void> reactionAddListener = gateway.getEventDispatcher().on(ReactionAddEvent.class)
                            .flatMap(event -> Mono.justOrEmpty(event.getEmoji().asCustomEmoji())
                                    .flatMap(emoji -> Flux.fromIterable(emojiHookMap.entrySet())
                                            .filter(entry -> emoji.getName().equals(entry.getKey()))
                                            .flatMap(entry -> entry.getValue().triggerAdded(event))
                                            .next()))
                            .then();

                    Mono<Void> reactionRemoveListener = gateway.getEventDispatcher().on(ReactionRemoveEvent.class)
                            .flatMap(event -> Mono.justOrEmpty(event.getEmoji().asCustomEmoji())
                                    .flatMap(emoji -> Flux.fromIterable(emojiHookMap.entrySet())
                                            .filter(entry -> emoji.getName().equals(entry.getKey()))
                                            .flatMap(entry -> entry.getValue().triggerRemoved(event))
                                            .next()))
                            .then();

                    return Mono.when(baddaBing, commandListener, reactionAddListener, reactionRemoveListener);
                })
                .block();
    }
}
