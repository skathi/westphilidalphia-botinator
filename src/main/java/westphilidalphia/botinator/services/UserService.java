package westphilidalphia.botinator.services;

import com.merakianalytics.orianna.Orianna;
import com.merakianalytics.orianna.types.common.Queue;
import com.merakianalytics.orianna.types.common.Season;
import com.merakianalytics.orianna.types.core.match.Match;
import com.merakianalytics.orianna.types.core.match.MatchHistory;
import com.merakianalytics.orianna.types.core.summoner.Summoner;
import discord4j.common.util.Snowflake;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import westphilidalphia.botinator.Botinator;
import westphilidalphia.botinator.models.User;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

public class UserService {
    private void saveOrUpdateUser(User user) {
        Session session = Botinator.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(user);
        transaction.commit();
    }

    private User getUserByDiscordId(long discordId) {
        Session session = Botinator.sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root).where(cb.equal(root.get("discordId"), discordId));
        Query<User> query = session.createQuery(cq);

        User user;
        try {
            user = query.getSingleResult();
        } catch (NoResultException e) {
            user = new User();
            user.setDiscordId(discordId);
        }

        return user;
    }

    public List<User> simpDollarScoreboard() {
        Session session = Botinator.sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root).where(cb.gt(root.get("simpDollars"), 0));
        Query<User> query = session.createQuery(cq);

        return query.getResultList();
    }

    public void simpDollarTransaction(Snowflake discordId, int amount) {
        User user = getUserByDiscordId(discordId.asLong());
        user.setSimpDollars(user.getSimpDollars() + amount);
        saveOrUpdateUser(user);
    }

    public Map<User, Integer> getUsersOnLosingStreaks() {
        Map<User, Integer> resultSet = new LinkedHashMap<>();

        Session session = Botinator.sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root).where(cb.isNotNull(root.get("summonerName")));
        Query<User> query = session.createQuery(cq);

        List<Map.Entry<User, Integer>> victimList = new ArrayList<>();
        for (User user : query.getResultList()) {
            int lostMatches = 0;
            Summoner summoner = Orianna.summonerNamed(user.getSummonerName()).get();
            MatchHistory matchHistory = summoner.matchHistory()
                    .withQueues(Queue.RANKED_SOLO)
                    .withSeasons(Season.getLatest())
                    .get();

            for (Match match : matchHistory) {
                if (
                        (match.getBlueTeam().getParticipants().contains(summoner)
                                && match.getBlueTeam().isWinner())
                                ||
                                (match.getRedTeam().getParticipants().contains(summoner)
                                        && match.getRedTeam().isWinner())
                ) {
                    break;
                } else {
                    lostMatches++;
                }
            }

            if (lostMatches > 1) {
                victimList.add(new AbstractMap.SimpleEntry<>(user, lostMatches));
            }
        }

        //spaghetti code? there's probably a better way to achieve all of this
        //having to build two data structures stinks
        victimList.stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(entry -> resultSet.put(entry.getKey(), entry.getValue()));

        return resultSet;
    }
}
