package westphilidalphia.botinator.services;

import org.hibernate.Session;
import org.hibernate.query.Query;
import westphilidalphia.botinator.Botinator;
import westphilidalphia.botinator.models.Keyword;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KeywordService {
    public List<Keyword> getDetectedKeywords(String messageContents) {
        List<String> parsedMessagesContents = Stream.of(messageContents.split(" "))
                .skip(1).collect(Collectors.toList());
        Session session = Botinator.sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Keyword> cq = cb.createQuery(Keyword.class);
        Root<Keyword> root = cq.from(Keyword.class);

        List<Predicate> predicateList = new ArrayList<>();
        for (String s : parsedMessagesContents) {
            predicateList.add(cb.equal(root.get("keyword"), s));
        }

        Predicate[] predicates = predicateList.toArray(new Predicate[0]);
        cq.select(root).where(cb.or(predicates));
        Query<Keyword> query = session.createQuery(cq);

        return query.getResultList();
    }
}
