package westphilidalphia.botinator.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

public interface RunnableCommand {
    Mono<Void> run(MessageCreateEvent event);
}
