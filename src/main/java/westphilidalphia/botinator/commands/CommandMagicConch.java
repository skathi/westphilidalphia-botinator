package westphilidalphia.botinator.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import reactor.core.publisher.Mono;
import westphilidalphia.botinator.Botinator;
import westphilidalphia.botinator.models.Keyword;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class CommandMagicConch implements RunnableCommand {
    @Override
    public Mono<Void> run(MessageCreateEvent event) {
        if (event.getMessage().getContent().split(" ").length > 1) {
            return event.getMessage().getAuthorAsMember()
                    .flatMap(member -> event.getMessage().getChannel()
                            .flatMap(messageChannel -> messageChannel
                                    .createMessage(askTheMagicConch(member.getId().asString(), event.getMessage()))))
                    .then();
        } else {
            return Mono.empty();
        }
    }

    private String askTheMagicConch(String tagId, Message message) {
        if (!message.getContent().isEmpty()) {
            String messageContents = message.getContent();
            List<Keyword> detectedKeywords = Botinator.keywordService.getDetectedKeywords(messageContents);
            if (!detectedKeywords.isEmpty()) {
                int confidenceCoefficient = 0;
                for (Keyword k : detectedKeywords) {
                    if (k.isRiggedValue()) {
                        ++confidenceCoefficient;
                    } else {
                        --confidenceCoefficient;
                    }
                }

                return "<@" + tagId + "> " + (confidenceCoefficient > 0 ? "yes" : "no");
            }
        }

        return "<@" + tagId + "> " + (ThreadLocalRandom.current().nextInt() % 2 == 0 ? "yes" : "no");
    }
}
