package westphilidalphia.botinator.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

public class CommandPing implements RunnableCommand {
    @Override
    public Mono<Void> run(MessageCreateEvent event) {
        return event.getMessage().getChannel()
                .flatMap(messageChannel -> messageChannel.createMessage("Pong!"))
                .then();
    }
}
