package westphilidalphia.botinator.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import org.jsoup.Jsoup;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class CommandShipStatus implements RunnableCommand {
    @Override
    public Mono<Void> run(MessageCreateEvent event) {
        Mono<String> wrapper = Mono.fromCallable(
                () -> Jsoup
                        .connect("https://istheboatstillstuck.com/")
                        .get()
                        .select("#message > h1")
                        .text()
        );
        wrapper = wrapper.subscribeOn(Schedulers.boundedElastic());

        return wrapper.flatMap(status -> event.getMessage().getAuthorAsMember()
                .flatMap(member -> event.getMessage().getChannel()
                        .flatMap(messageChannel -> messageChannel
                                .createMessage("<@" + member.getId().asString() + "> " + status))))
                .then();
    }
}
