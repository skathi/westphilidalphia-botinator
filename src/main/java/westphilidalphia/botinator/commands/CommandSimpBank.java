package westphilidalphia.botinator.commands;

import discord4j.common.util.Snowflake;
import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;
import westphilidalphia.botinator.Botinator;
import westphilidalphia.botinator.models.User;

import java.util.Comparator;
import java.util.List;

public class CommandSimpBank implements RunnableCommand {
    @Override
    public Mono<Void> run(MessageCreateEvent event) {
        return event.getMessage().getChannel()
                .flatMap(messageChannel -> messageChannel
                        .createMessage(buildScoreboard(event)))
                .then();
    }

    private String buildScoreboard(MessageCreateEvent event) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("SimpBank Scoreboard:").append(System.getProperty("line.separator"));
        List<User> users = Botinator.userService.simpDollarScoreboard();
        users.sort(Comparator.comparingInt(User::getSimpDollars).reversed());

        users.forEach(user -> event.getGuild()
                .subscribe(guild -> guild.getMemberById(Snowflake.of(user.getDiscordId()))
                        .onErrorResume(e -> Mono.empty())
                        .subscribe(member -> buffer
                                .append(member.getDisplayName())
                                .append(": ")
                                .append(user.getSimpDollars())
                                .append(System.getProperty("line.separator")))));

        return buffer.toString();
    }
}
