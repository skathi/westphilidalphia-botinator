package westphilidalphia.botinator.commands;

import discord4j.common.util.Snowflake;
import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;
import westphilidalphia.botinator.Botinator;
import westphilidalphia.botinator.models.User;

import java.util.Map;

public class CommandLosingStreak implements RunnableCommand {
    @Override
    public Mono<Void> run(MessageCreateEvent event) {
        return event.getMessage().getChannel()
                .flatMap(messageChannel -> messageChannel
                        .createMessage(buildLosingStreakAnnouncement(event)))
                .then();
    }

    private String buildLosingStreakAnnouncement(MessageCreateEvent event) {
        Map<User, Integer> victims = Botinator.userService.getUsersOnLosingStreaks();

        if (victims.isEmpty()) {
            return "No one's on a losing streak... Yet!";
        }

        StringBuilder buffer = new StringBuilder();
        buffer.append("A round of applause for the following people!")
                .append(System.getProperty("line.separator"));

        event.getGuild().subscribe(guild -> {
            for (Map.Entry<User, Integer> entry : victims.entrySet()) {
                guild.getMemberById(Snowflake.of(entry.getKey().getDiscordId()))
                        .subscribe(member -> buffer.append(member.getDisplayName())
                                .append(" is on a ")
                                .append(entry.getValue())
                                .append(" game losing streak")
                                .append(System.getProperty("line.separator")));
            }
        });

        return buffer.toString();
    }
}
