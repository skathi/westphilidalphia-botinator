package westphilidalphia.botinator.models;

import javax.persistence.*;

@Entity
@Table(name = "botinator_users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "simpdollars")
    private int simpDollars;

    @Column(name = "discord_id")
    private long discordId;

    @Column(name = "summoner_name")
    private String summonerName;

    public User() {
    }

    public User(int id, int simpDollars, long discordId, String summonerName) {
        this.id = id;
        this.simpDollars = simpDollars;
        this.discordId = discordId;
        this.summonerName = summonerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSimpDollars() {
        return simpDollars;
    }

    public void setSimpDollars(int simpDollars) {
        this.simpDollars = simpDollars;
    }

    public long getDiscordId() {
        return discordId;
    }

    public void setDiscordId(long discordId) {
        this.discordId = discordId;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public void setSummonerName(String summonerName) {
        this.summonerName = summonerName;
    }
}
