package westphilidalphia.botinator.models;

import javax.persistence.*;

@Entity
@Table(name = "keywords")
public class Keyword {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "keyword")
    private String keyword;

    @Column(name = "rigged_value")
    private boolean riggedValue;

    public Keyword() {
    }

    public Keyword(int id, String keyword, boolean riggedValue) {
        this.id = id;
        this.keyword = keyword;
        this.riggedValue = riggedValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public boolean isRiggedValue() {
        return riggedValue;
    }

    public void setRiggedValue(boolean riggedValue) {
        this.riggedValue = riggedValue;
    }
}
